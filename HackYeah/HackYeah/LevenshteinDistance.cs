﻿using System;

namespace HackYeah
{
    public class LevenshteinDistance
    {
        public const int Satisfying = 50;

        public static bool AreStringsSimilar(string s, string t)
        {
            return GetFactor(s, t) < Satisfying;
        }

        // im większy tym gorzej
        public static int GetFactor(string s, string t)
        {

            if (string.IsNullOrEmpty(s))
            {
                if (string.IsNullOrEmpty(t))
                    return 0;
                return 100;
            }

            if (string.IsNullOrEmpty(t))
            {
                return 100;
            }

            s = s.ToLower();
            t = t.ToLower();
            
            if (s.Contains(t) || t.Contains(s))
            {
                return 0;
            }

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // initialize the top and right of the table to 0, 1, 2, ...
            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 1; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            var result = ((100 * d[n, m]) / n); //jeśli ponad 100 procent to bardzo niepodobne są;
            return result > 100 ? 100 : result;
        }
    }
}