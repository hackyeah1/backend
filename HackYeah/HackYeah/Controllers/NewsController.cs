﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackYeah.Controllers.Models;
using HackYeah.Repository;
using HackYeah.Repository.Models;
using HackYeah.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HackYeah.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class NewsController : ControllerBase
    {
        private readonly ILogger<NewsController> _logger;
        private readonly NewsRepository _reportRepository;

        public NewsController(ILogger<NewsController> logger, NewsRepository reportRepository)
        {
            _logger = logger;
            _reportRepository = reportRepository;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(NewsPayload), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(string id)
        {
            var report = await _reportRepository.Get(id);
            if (report == null) { return NotFound(); }

            return Ok(NewsPayload.CreateNew(report));
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<NewsPayload>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(string type, string content, string opinion)
        {
            IEnumerable<News> result;

            if (string.IsNullOrEmpty(type + content))
            {
                result = await _reportRepository.GetAll();
            }
            else if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(content))
            {
                result = await _reportRepository.Get(type, content);
            }
            else if (!string.IsNullOrEmpty(type))
            {
                result = await _reportRepository.GetByType(type);
            }
            else
            {
                result = await _reportRepository.GetByContent(content);
            }

            if (!string.IsNullOrEmpty(opinion))
            {
                result = result.Where(x => x.Opinion == opinion);
            }

            return Ok(result.Select(x => NewsPayload.CreateNew(x)));
        }

        [HttpPost]
        [ProducesResponseType(typeof(NewsPayload), StatusCodes.Status201Created)]
        public async Task<IActionResult> Create([FromBody] NewsPayload payload)
        {
            var report = await _reportRepository.SaveAsync(News.CreateNew(payload.Type, payload.Content));
            return Ok(NewsPayload.CreateNew(report));
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(string id)
        {
            await _reportRepository.DeleteAsync(id);
            return Ok();
        }

        [HttpPatch]
        [Route("webmethods/{id}/makeItFake")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> MakeItFake(string id)
        {
            var report = await _reportRepository.Get(id);
            report.FakeCount++;
            await _reportRepository.SaveAsync(report);

            return Ok();
        }

        [HttpPatch]
        [Route("webmethods/{id}/makeItTruth")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> MakeItTruth(string id)
        {
            var report = await _reportRepository.Get(id);
            report.TruthCount++;
            await _reportRepository.SaveAsync(report);

            return Ok();
        }

        [HttpGet]
        [Route("webmethods/verify")]
        [ProducesResponseType(typeof(NewsPayload), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Verify(string type, string content)
        {
            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(content))
            {
                return BadRequest("content and type are required");
            }

            var news = await _reportRepository.GetSingleOrDefault(type, content);

            if (news == null) { return NotFound(); }

            return Ok(NewsPayload.CreateNew(news));
        }
    }
}
