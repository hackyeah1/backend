﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackYeah.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class MonitorController : ControllerBase
    {
        public static DateTime startDate = DateTime.Now;

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok($"Serwis działa od {startDate}. Teraz jest {DateTime.Now}");
        }
    }
}
