﻿using System;
using HackYeah.Repository.Models;

namespace HackYeah.Controllers.Models
{
    public class NewsPayload
    {
        public string Id { get; set; }
        public string Type { get; set; } // text | link | file
        public string Content { get; set; }
        public string Opinion { get; set; }
        private int _truthCount;
        public int TruthCount { get => _truthCount; set => _truthCount = value > 5 ? 5 : value; }
        private int _fakeCount;
        public int FakeCount { get => _fakeCount; set => _fakeCount = value > 5 ? 5 : value; }

        public static NewsPayload CreateNew(News news)
             => new NewsPayload
             {
                 Id = news.Id,
                 Type = news.Type,
                 Content = news.Content,
                 Opinion = news.Opinion,
                 TruthCount = news.TruthCount,
                 FakeCount = news.FakeCount,
             };
    }
}

