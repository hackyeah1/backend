﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using HackYeah.Repository.Models;
using HackYeah.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackYeah.Repository
{
    public class NewsRepository
    {
        private readonly DynamoDbService _dynamoDbService;

        public NewsRepository(DynamoDbService dynamoDbService)
        {
            _dynamoDbService = dynamoDbService;
        }

        public async Task<News> Get(string id)
        {
            return await _dynamoDbService.GetAsync<News>(id);
        }

        public async Task<News> GetSingleOrDefault(string type, string content)
        {
            if (type == "text")
            {
                var allReports = await GetByType("text");
                var result = new List<(int factor, News news)>();
                foreach (var item in allReports)
                {
                    result.Add((LevenshteinDistance.GetFactor(content, item.Content), item));
                }
                var similaries = result.Where(x => x.factor <= LevenshteinDistance.Satisfying);
                if (similaries.Any())
                {
                    return similaries.OrderBy(x => x.factor).First().news;
                }

                return null;
            }
            else
            {
                return (await GetAll()).Where(x => x.Type == type && x.Content == content).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<News>> Get(string type, string content)
        {
            if (type == "text")
            {
                var textsNews = await GetByType("text");
                var result = new List<News>();
                foreach (var item in textsNews)
                {
                    if (LevenshteinDistance.AreStringsSimilar(content, item.Content))
                    {
                        result.Add(item);
                    }
                }
                return new List<News>(result);
            }
            else
            {
                return new List<News>(await GetAll()).Where(x => x.Type == type && x.Content == content);
            }
        }

        public async Task<IEnumerable<News>> GetAll()
        {
            return await _dynamoDbService.GetAllAsync<News>();
        }

        public async Task<IEnumerable<News>> GetByType(string type)
        {
            return new List<News>(await GetAll()).Where(x => x.Type == type);
        }

        public async Task<IEnumerable<News>> GetByContent(string content)
        {
            var allReports = await GetByType("text");
            var result = new List<News>();
            foreach (var item in allReports)
            {
                if (LevenshteinDistance.AreStringsSimilar(content, item.Content))
                {
                    result.Add(item);
                }
            }
            return new List<News>(result);
        }

        public async Task<News> SaveAsync(News report)
        {
            return await _dynamoDbService.WriteAsync<News>(report);
        }

        public async Task DeleteAsync(string id)
        {
            var report = await Get(id);
            if (report == null) { return; }

            await _dynamoDbService.DeleteAsync<News>(report);
        }
    }
}
