﻿using Amazon.DynamoDBv2.DataModel;
namespace HackYeah.Repository.Models
{
    [DynamoDBTable("User")]
    public class User
    {
        [DynamoDBHashKey]
        public string Id { get; set; }

        [DynamoDBProperty]
        public string Email { get; set; }
    }
}
