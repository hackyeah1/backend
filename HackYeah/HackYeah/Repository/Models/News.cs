﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackYeah.Repository.Models
{
    [DynamoDBTable("News")]
    public class News
    {
        [DynamoDBHashKey]
        public string Id { get; set; }

        [DynamoDBProperty]
        public string Type { get; set; } // text | link | file

        [DynamoDBProperty]
        public string Content { get; set; }

        [DynamoDBProperty("ThruthCount")]
        public int TruthCount { get; set; }

        [DynamoDBProperty]
        public int FakeCount { get; set; }

        [DynamoDBIgnore]
        public string Opinion { get => FakeCount <5 && TruthCount < 5 ? "unconfirmed" : FakeCount - TruthCount >= 5 ? "fake" : "truth"; }

        public static News CreateNew(string type, string content)
            => new News
            {
                Id = Guid.NewGuid().ToString().Replace("-", ""),
                Type = type,
                Content = content
            };
    }
}
