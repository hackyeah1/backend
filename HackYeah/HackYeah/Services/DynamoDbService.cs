﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackYeah.Services
{
    public class DynamoDbService
    {
        private readonly IAmazonDynamoDB _dynamoDbClient;
        public DynamoDbService(IAmazonDynamoDB client)
        {
            _dynamoDbClient = client;
        }

        public async Task Seed()
        {
            var list = await _dynamoDbClient.ListTablesAsync();

            string userTable = "Users";
            if (list.TableNames.Contains(userTable)) await _dynamoDbClient.DeleteTableAsync("Users");

            CreateTableRequest usersRequest = new CreateTableRequest()
            {
                TableName = userTable,
                AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition("Id", ScalarAttributeType.S)
                    },
                KeySchema = new List<KeySchemaElement>()
                    {
                         new KeySchemaElement("Id", KeyType.HASH)
                    },
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = 5,
                    WriteCapacityUnits = 6
                }
            };

            await _dynamoDbClient.CreateTableAsync(usersRequest);


            string newsTable = "News";
            if (list.TableNames.Contains(newsTable)) await _dynamoDbClient.DeleteTableAsync(newsTable);
            CreateTableRequest newsRequest = new CreateTableRequest()
            {
                TableName = newsTable,
                AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition("Id", ScalarAttributeType.S)
                    },
                KeySchema = new List<KeySchemaElement>()
                    {
                         new KeySchemaElement("Id", KeyType.HASH)
                    },
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = 5,
                    WriteCapacityUnits = 6
                }
            };

            await _dynamoDbClient.CreateTableAsync(newsRequest);
        }

        public async Task<T> GetAsync<T>(string id)
        {
            try
            {
                DynamoDBContext context = new DynamoDBContext(_dynamoDbClient);
                return await context.LoadAsync<T>(id);
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Get operation! Error: {ex}");
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>()
        {
            try
            {
                DynamoDBContext context = new DynamoDBContext(_dynamoDbClient);
                return await context.ScanAsync<T>(new List<ScanCondition>()).GetRemainingAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Get operation! Error: {ex}");
            }
        }

        public async Task<IEnumerable<T>> FindAsync<T>(List<ScanCondition> conditions)
        {
            try
            {
                DynamoDBContext context = new DynamoDBContext(_dynamoDbClient);

                return await context.ScanAsync<T>(conditions).GetRemainingAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Get operation! Error: {ex}");
            }
        }

        public async Task<T> WriteAsync<T>(T item)
        {
            try
            {
                DynamoDBContext context = new DynamoDBContext(_dynamoDbClient);
                await context.SaveAsync(item);
                return item;
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Write operation! Error: {ex}");
            }
        }

        public async Task DeleteAsync<T>(T item)
        {
            try
            {
                DynamoDBContext context = new DynamoDBContext(_dynamoDbClient);
                await context.DeleteAsync(item);
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Delete operation! Error: {ex}");
            }
        }
    }
}